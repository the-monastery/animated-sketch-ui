package com.ghostmonk.events
{
	import com.ghostmonk.media.video.IVideoPlayer;
	
	import flash.events.Event;

	public class VideoPlayerEvent extends Event
	{
		public static const READY:String = "ready";
		public static const UPDATE:String = "update";
		public static const LOADING:String = "loading";
		public static const FINISHED:String = "finished";
		public static const PLAY:String = "play";
		public static const STOP:String = "stop";
		public static const PAUSE:String = "pause";
		public static const BUFFERING_STARTED:String = "buffering started";
		public static const BUFFERING_STOPPED:String = "buffering stopped";
		
		private var _player:IVideoPlayer;
		
		public function get player():IVideoPlayer
		{
			return _player;
		}
		
		public function VideoPlayerEvent(event: String, player: IVideoPlayer = null)
		{
			super(event);			
			_player = player;
		}
	}
}