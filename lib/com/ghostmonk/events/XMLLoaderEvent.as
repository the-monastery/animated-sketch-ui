package com.ghostmonk.events
{
	import com.ghostmonk.net.XMLLoader;
	
	import flash.events.Event;

	public class XMLLoaderEvent extends Event
	{
		public static var PROGRESS:String = "progress";
		
		private var _xmlLoader:XMLLoader;
		
		public function get xmlLoader():XMLLoader
		{
			return _xmlLoader;
		}
		
		public function XMLLoaderEvent(type:String, xmlLoader:XMLLoader, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			_xmlLoader = xmlLoader;
		}
		
	}
}