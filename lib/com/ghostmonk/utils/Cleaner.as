package com.ghostmonk.utils
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	public class Cleaner
	{
		public static var throwErrors:Boolean = false;
		
		public static function removeChild(container:DisplayObjectContainer, child:DisplayObject):void
		{
			if(container)
			{
				if(child)
				{
					if(container.contains(child))
					{
						container.removeChild(child);
					}
					else if(throwErrors)
					{
						throw new Error("Cleaner Error: container does not contain child");
					}
				}
				else if(throwErrors)
				{
					throw new Error("Cleaner Error: child is null");
				}
			}
			else if(throwErrors)
			{
				throw new Error("Cleaner Error: container is null");
			}
		}

	}
}