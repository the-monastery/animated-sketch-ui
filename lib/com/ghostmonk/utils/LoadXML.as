package com.ghostmonk.utils
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	public class LoadXML extends EventDispatcher
	{
		private var _urlLoader:URLLoader;
		private var _xml:XML;
		
		public function get xml():XML{
			return _xml;
		}
		
		public function LoadXML(url:String)
		{
			_urlLoader = new URLLoader();
			_urlLoader.addEventListener(Event.COMPLETE, onComplete);
			_urlLoader.load(new URLRequest(url));
		}
		
		private function onComplete(e:Event):void
		{
			_xml = new XML(e.target.data);
			dispatchEvent(new Event(Event.COMPLETE));
		}

	}
}