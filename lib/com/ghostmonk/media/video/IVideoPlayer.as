package com.ghostmonk.media.video
{
	public interface IVideoPlayer
	{
		
		function set width(width:Number):void;	
		function get width():Number;
	
		function set height(height:Number):void;
		function get height():Number;

		function set lockAspectRatio(value: Boolean):void;
		function get lockAspectRatio():Boolean;
		
		function set autoPlay(value: Boolean):void;
		function get autoPlay():Boolean;
		
		function get duration():Number;
		
		function get position():Number;

		function set volume(value:Number):void;
		function get volume():Number;

		function set bufferTime(time: Number):void;
		function get bufferTime():Number;
	
		function get bytesTotal():uint;
		
		function get bytesLoaded():uint;
		
		function set title(value:String):void;
		function get title():String;
		
		function load(videoURL:String):void
		
		function play():void
			
		function pause():void
			
		function togglePause():void
		
		function stop():void
		
		function scrub(percentage:Number):void
		
		function mute():void
				
		function toggleMute():void
	}
}