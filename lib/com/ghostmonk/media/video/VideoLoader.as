﻿package com.champagneValentine.workPortfolio.level2{
	
	import caurina.transitions.Tweener;
	import caurina.transitions.Type;
	
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import flash.media.*;
	import flash.net.*;
	import flash.text.*;
	
	public class VideoLoader extends MovieClip{
		
		private var _scaleVideoBtn:Sprite = new Sprite();
		private var _reduceVideoBitmap:BitmapData = new ReduceVideoBtn(0,0);
		private var _reduceVideoImage:Bitmap = new Bitmap(_reduceVideoBitmap);
		private var _enlargeVideoBitmap:BitmapData = new EnlargeVideoBtn(0,0);
		private var _enlargeVideoImage:Bitmap = new Bitmap(_enlargeVideoBitmap);
		
		private var _bufferBitmap:BitmapData = new Buffering(0,0);
		private var _bufferImage:Bitmap = new Bitmap(_bufferBitmap);
		
		private var _border:Sprite = new Sprite();
		private var _clickToPlayBitmap:BitmapData = new ClickToPlay(0,0); 
		private var _clickToPlayImage:Bitmap = new Bitmap(_clickToPlayBitmap);
		private	var _logo:Loader = new Loader();
		
		private var _connection:NetConnection = new NetConnection();
		private var _stream:NetStream;
		private var _video:Video = new Video(373,280);
		private var _screen:Sprite = new Sprite();
		private var _duration:Number;
		private var _metadataObj:Object = new Object();
		
		public static const PLAY_READY:String = "playready";
		
		public function VideoLoader()
		{
	
			_border.graphics.beginFill(0x554646, 1);
			_border.graphics.drawRect(0,0,379,286);
			_border.graphics.endFill();
			
			_screen.graphics.beginFill(0x000000, 0.8);
			_screen.graphics.drawRect(0,0,373,280);
			_screen.graphics.endFill();
			
			_connection.connect(null);
			_stream = new NetStream(_connection);
			_video.attachNetStream(_stream);

			_border.buttonMode = true;
			_scaleVideoBtn.addChild(_enlargeVideoImage);
			
			addChild(_border);
			_border.addChild(_video);
			_border.addChild(_screen);
			_screen.addChild(_clickToPlayImage);
			_screen.addChild(_bufferImage);
			_bufferImage.alpha = 0;
			_screen.addChild(_logo);
			addChild(_scaleVideoBtn);
			
			_scaleVideoBtn.x = _border.width - (_enlargeVideoImage.width - 10);
			_scaleVideoBtn.y = -(_enlargeVideoImage.height - 3);
			_video.x = 3;
			_video.y = 3;
			_screen.x = _video.x;
			_screen.y = _video.y;
			_bufferImage.x = (_screen.width - _bufferImage.width)/2;
			_bufferImage.y = (_screen.height - _bufferImage.height)/2;
			_scaleVideoBtn.buttonMode = true;
			
			_scaleVideoBtn.addEventListener(MouseEvent.CLICK, enlargeVideo);
			
			_logo.mouseEnabled = false;
			_screen.mouseEnabled = false;
			
		}
		
		internal function videoAssets(videoURL:String, logoURL:String):void
		{
			_video.clear();
			_stream.play(videoURL);
			_stream.pause();
			_stream.seek(0);
			_stream.client = _metadataObj;
			_logo.alpha = 0;
			_clickToPlayImage.alpha =0;
			_bufferImage.alpha = 1;
			
			_metadataObj.onMetaData = function(metaData:Object):void{
				removeEventListener(Event.ENTER_FRAME, activateConsol);
				_duration = metaData.duration;
				addEventListener(Event.ENTER_FRAME, activateConsol);
				var bufferIn:Boolean = false;
				function activateConsol(e:Event):void{
					
					//this conditional animates buffer image
					if(bufferIn)
					{
						_bufferImage.alpha+= 0.05
						if(_bufferImage.alpha >= 1)
						{
							bufferIn = false;
						}
					}
					else
					{
						_bufferImage.alpha-=0.05;
						if(_bufferImage.alpha <= 0)
						{
							bufferIn = true;
						}
					}
					
					if(_stream.bytesLoaded >= _stream.bytesTotal/10)
					{
						dispatchEvent(new Event(PLAY_READY));
						removeEventListener(Event.ENTER_FRAME, activateConsol);
						_bufferImage.alpha = 0;
						_logo.alpha = 1;
						_clickToPlayImage.alpha = 1;
						_border.addEventListener(MouseEvent.CLICK, videoControl);
						_border.addEventListener(MouseEvent.MOUSE_OVER, videoControl);
						_border.addEventListener(MouseEvent.MOUSE_OUT, videoControl);
					}
				}
			}
			_logo.unload();
			_logo.load(new URLRequest(logoURL));
			//this is used so the format video function is only called after the width and height variables are known
			_logo.contentLoaderInfo.addEventListener(Event.INIT, formatVideo);
			
		}
		
		private function formatVideo(e:Event):void
		{
			_logo.x = ((_border.width - (_logo.contentLoaderInfo.width * _border.scaleX))/2)/_border.scaleX;
			_logo.y = ((_border.height - (_logo.contentLoaderInfo.height * _border.scaleY))/2)/_border.scaleY;
			
			_clickToPlayImage.x = ((_border.width - (_clickToPlayImage.width * _border.scaleX))/2)/_border.scaleX;;
			_clickToPlayImage.y = _logo.y + (_logo.contentLoaderInfo.height) + (20);
			_logo.contentLoaderInfo.removeEventListener(Event.INIT, formatVideo);
		}
		
		private function playVideo(e:MouseEvent):void
		{
			_stream.seek(0);
			_stream.resume();
		}
		
		private function stopVideo(e:MouseEvent):void
		{
			_stream.pause();
			_stream.seek(0);
		}
		
		private function trackVideo(e:Event):void
		{
			if(_stream.time >= _duration)
			{
				_stream.pause();
				_stream.seek(0);
				videoPlaying = false;
				addScreen(e);
			}
		}
		
		private function enlargeVideo(e:MouseEvent):void
		{
			Tweener.addTween(_border, {scaleX:1.93, scaleY:1.93, time:1, transition:Type.QUAD_IN});
			Tweener.addTween(_scaleVideoBtn, {x:_scaleVideoBtn.x + 347, time:1, transition:Type.QUAD_IN});
			_scaleVideoBtn.removeChild(_enlargeVideoImage);
			_scaleVideoBtn.addChild(_reduceVideoImage);
			_scaleVideoBtn.addEventListener(MouseEvent.CLICK, reduceVideo);
			_scaleVideoBtn.removeEventListener(MouseEvent.CLICK, enlargeVideo);
		}
		
		private function reduceVideo(e:MouseEvent):void
		{
			Tweener.addTween(_border, {scaleX:1, scaleY:1, time:1, transition:Type.QUAD_IN});
			Tweener.addTween(_scaleVideoBtn, {x:_scaleVideoBtn.x - 347, time:1, transition:Type.QUAD_IN});
			_scaleVideoBtn.addChild(_enlargeVideoImage);
			_scaleVideoBtn.removeChild(_reduceVideoImage);
			_scaleVideoBtn.removeEventListener(MouseEvent.CLICK, reduceVideo);
			_scaleVideoBtn.addEventListener(MouseEvent.CLICK, enlargeVideo);
		}
		
		private function addScreen(e:Event):void
		{
			Tweener.addTween(_screen, {alpha:1, time:0.3, transition:Type.QUAD_IN});
		}
		
		private function clearScreen(e:Event):void
		{
			Tweener.addTween(_screen, {alpha:0, time:0.3, transition:Type.QUAD_IN});
		}
		
		private var videoPlaying:Boolean = false;
		private function videoControl(e:MouseEvent):void
		{
			
			if(videoPlaying)
			{
				switch(e.type)
				{
					case MouseEvent.MOUSE_OVER:
					break;
					
					case MouseEvent.MOUSE_OUT:
					break;
					
					case MouseEvent.CLICK:
					stopVideo(e);
					videoPlaying = false;
					_border.removeEventListener(Event.ENTER_FRAME, trackVideo);
					break;
				}
			}
			else
			{
				switch(e.type)
				{
					case MouseEvent.MOUSE_OVER:
					clearScreen(e);
					break;
					
					case MouseEvent.MOUSE_OUT:
					addScreen(e);
					break;
					
					case MouseEvent.CLICK:
					playVideo(e);
					videoPlaying = true;
					_border.addEventListener(Event.ENTER_FRAME, trackVideo);
					clearScreen(e);
					break;
				}
			}
			
		}
		
		internal function resetAssets(videoReset:Boolean = true):void
		{
			stopVideo(null);
			videoReset ? resetVideo() : "" ;
			_video.clear();
			_border.removeEventListener(MouseEvent.CLICK, videoControl);
			_border.removeEventListener(MouseEvent.MOUSE_OVER, videoControl);
			_border.removeEventListener(MouseEvent.MOUSE_OUT, videoControl);
			_border.removeEventListener(Event.ENTER_FRAME, trackVideo);
			videoPlaying = false;
			addScreen(null);
			
		}
		
		internal function resetVideo():void
		{
			_scaleVideoBtn.x = 379 - (_enlargeVideoImage.width - 10);
			_border.scaleX = 1;
			_border.scaleY = 1;
			
			try
			{
				//if the first statement fails (reduce video image isn't there), then nothing runs
				_scaleVideoBtn.removeChild(_reduceVideoImage);
				_scaleVideoBtn.addChild(_enlargeVideoImage);
				_scaleVideoBtn.removeEventListener(MouseEvent.CLICK, reduceVideo);
				_scaleVideoBtn.addEventListener(MouseEvent.CLICK, enlargeVideo);
			}
			catch(e:Error){}		
		}
		
	}	
}