package com.ghostmonk.media.audio
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	
	public class SoundManager
	{
		static private var _instance:SoundManager; 
		private var _soundTransform:SoundTransform;
		
		public function SoundManager(){
			
			_soundTransform = new SoundTransform(); 
		
		}

		public function playSound(soundClass:Class, soundInfo:Object = null):Object {
			
			var sound:Sound = new soundClass() as Sound;
			
			var startTime:Number = 0;
			var loops:Number = 0;
			var volume:Number = 1;
			var pan:Number = 0;
			
			if(soundInfo != null){
				
				if(soundInfo.startTime != undefined){
					startTime = soundInfo.startTime;
				}
				
				if(soundInfo.loops != undefined){
					loops = soundInfo.loops;
				}
				
				if(soundInfo.volume != undefined){
					volume = soundInfo.volume;
				}
				
				if(soundInfo.pan  != undefined){
					pan = soundInfo.pan;
				}
			}

			var soundTransform:SoundTransform = new SoundTransform(volume*_soundTransform.volume, pan*_soundTransform.pan);

			var channel:SoundChannel = sound.play(startTime,loops,soundTransform); 
			
			return {sound:sound, transform:soundTransform, channel:channel};
			 
		}		
		
		static public function getInstance():SoundManager {
			if(_instance == null){
				return new SoundManager();
			} else {
				return _instance;
			}
		}
		


	}
} 