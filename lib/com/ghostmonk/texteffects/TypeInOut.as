package com.ghostmonk.texteffects
{
	import caurina.transitions.Tweener;
	
	import flash.display.Sprite;
	import flash.text.TextField;
	
	public class TypeInOut
	{
		// ----
		public static const OVER:String = "over";
		public static const SHUFFLE:String = "shuffle";
		public static const SHUFFLE_PUSH:String = "shufflPush";
		public static const BACKTRACK:String = "backtrack";
		public static const TEST:String = "test";
		
		public function TypeInOut(){}
		
		public static function addTween(tf:TextField, tweenParams:Object):void
		{
			
			var text:String = tf.text;
			var textLength:Number = text.length;
			var newColor:String;
			var oldColor:String;
			var type:String;
			var destText:String;
			var tweenHolder:Object = {tweenAmount:0};
			
			//destText = (tweenParams.text) ? tweenParams.text : text;
			
			tweenParams.text 		? destText 	= 	tweenParams.text 		: 	destText = text;	
			tweenParams.type 		? type 		= 	tweenParams.type 		: 	type = OVER;
			tweenParams.newColor 	? newColor 	= 	tweenParams.newColor	: 	newColor = "75BA59"; 
			tweenParams.oldColor 	? oldColor	= 	tweenParams.oldColor	: 	oldColor = "999999";			
			
			var tweenObject:Object = {	tweenAmount:1, 
										onUpdate:updateText,
										onUpdateParams:[tf, tweenHolder, type, text, destText, newColor, oldColor]};
			
			for(var key:String in tweenParams){
				
				if(key != "type" && key != "text" && key != "onUpdate" && key != "onUpdateParams" && key != "newColor" && key != "oldColor"){
					tweenObject[key] = tweenParams[key];
				}
				
			}
			
			Tweener.addTween(tweenHolder, tweenObject); 
			
		}
		
		private function dispatchComplete():void{
			
		}
		
		private static function updateText(tf:TextField, tweenHolder:Object, type:String, text:String, destText:String, newColor:String, oldColor:String):void{
		
			var amount:Number = tweenHolder.tweenAmount;
			
			var newString:String = "";
			var oldString:String = "";
			
			switch(type){
	
				case OVER:
					newString = destText.slice(0,(amount)*destText.length);
					oldString = destText.slice((amount)*text.length,text.length);
				break;
				
				case SHUFFLE:
					newString = destText.slice((1-amount)*text.length,text.length);
					oldString = destText.slice(0,(1-amount)*destText.length);
				break;
				
				case SHUFFLE_PUSH:
					newString = destText.slice((1-amount)*text.length,text.length);
					oldString = destText.slice(0,(1-amount)*destText.length + ((1-amount)*destText.length));
				break;

				case BACKTRACK:
					newString = destText.slice((amount)*text.length,text.length);
					oldString = destText.slice(0,(amount)*destText.length);
				break;
				
				case TEST:
					newString = destText.slice(0,(amount)*destText.length);
					oldString = destText.slice((amount)*text.length,text.length);
				break;
			
			}
			
			if ( type != TEST )
			{
				tf.htmlText = '<font color="#'+newColor+'">' + newString + '</font><font color="#'+oldColor+'">' + oldString + '</font>';
			}
			else
			{
				tf.htmlText = newString;
			}
			
		}



	}
}