package {
	import caurina.transitions.properties.DisplayShortcuts;
	import caurina.transitions.properties.TextShortcuts;
	
	import com.ghostmonk.utils.LoadXML;
	import com.notsosimpleton.GlobalMenu;
	import com.notsosimpleton.SectionsManager;
	import com.notsosimpleton.data.ProjectData;
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.type.URLButton;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;

	[SWF (width=970, height=600, backgroundColor=0xffffff, frameRate=31)]

	public class NotSoSimpleton extends Sprite
	{
		private var _mediaTempleLink:AbstractButton;
		private var _ghostMonkLink:AbstractButton;
		private var _copyright:MovieClip = new Copyright();
		private var _menu:MovieClip = new GlobalMenu();
		private var _loadXML:LoadXML;
		
		private var _sectionManager:SectionsManager;
		
		public function NotSoSimpleton()
		{
			DisplayShortcuts.init();
			TextShortcuts.init();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			//All Assets using Tweener shortcuts need to be declared after the init() function calls
			_mediaTempleLink = new URLButton(MediaTemple, "http://www.mediatemple.org");
			_ghostMonkLink = new URLButton(_copyright.ghost, "http://selectedwork.ghostmonk.com");
			_copyright.addChild(_ghostMonkLink);
		}
		
		private function onAddedToStage(e:Event):void
		{
			_loadXML = new LoadXML("flashData/xml/config.xml");
			_loadXML.addEventListener(Event.COMPLETE, onXMLComplete);
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onXMLComplete(e:Event):void
		{
			removeEventListener(Event.COMPLETE, onXMLComplete);
			ProjectData.setXML(_loadXML.xml);
			
			_sectionManager  = new SectionsManager();
			
			onStageResize(null);
			
			addChild(_menu);
			addChild(_mediaTempleLink);
			
			addChild(_copyright);
			addChild(_sectionManager);
			
			stage.addEventListener(Event.RESIZE, onStageResize);
		}
		
		private function onStageResize(e:Event):void
		{
			_mediaTempleLink.y = 15;
			_mediaTempleLink.x = 15;
			
			_menu.x = stage.stageWidth - 500;
			_menu.y = _mediaTempleLink.y;
			
			_copyright.x = (stage.stageWidth - _copyright.width)/2;
			_copyright.y = stage.stageHeight - _copyright.height - 10;
			
			_sectionManager.reposition();
			
			
		}
		
	}
}
