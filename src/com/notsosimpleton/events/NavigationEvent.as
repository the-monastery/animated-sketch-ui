package com.notsosimpleton.events
{
	import flash.events.Event;

	public class NavigationEvent extends Event
	{
		public static const BUILD_OUT_COMPLETE:String = "buildOutComplete";
		public static const BUILD_IN_COMPLETE:String = "buildInComplete";
		public static const NAVIGATE_TO_SECTION:String = "navigateToSection";
		
		public static const FULL_SCREEN:String = "fullScreen";
		
		public static const PREVIOUS_CLIENT:String = "previousClient";
		public static const NEXT_CLIENT:String = "nextClient";
		
		public static const PREVIOUS_PROJECT:String = "previousProject";
		public static const NEXT_PROJECT:String = "nextProject";
		
		public static const ONE:String = "one";
		public static const TWO:String = "two";
		public static const THREE:String = "three";
		public static const FOUR:String = "four";
		public static const FIVE:String = "five";
		public static const SIX:String = "six";
		
		public static const PREVIOUS_PHOTO:String = "previousPhoto";
		public static const NEXT_PHOTO:String = "nextPhoto";
		
		public static const NAVIGATE_TO_PAGE:String = "navigateToPage";
		
		public static const MODULE_CLICKED:String = "moduleClicked";
		
		public static const CLOSE:String = "close";
		
		private var _index:String;
		
		public function NavigationEvent(type:String, index:String = null)
		{
			super(type);
			
			_index = index;
		}
		
		public function get index():String
		{
			return _index;
		}
		
	}
}