package com.notsosimpleton.sections
{
	public class SectionReference
	{
		public static const CURRENT_AFFAIRS:String = "currentAffairs";
		public static const ABOUT:String = "about";
		public static const SHOWCASE:String = "showcase";
		public static const SPLASH_SCREEN:String = "splashScreen"; 
		
		public function SectionReference(){}

	}
}