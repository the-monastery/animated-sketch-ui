package com.notsosimpleton.sections.type
{
	//Embarassingly huge class... needs to be cut up a little... a lot
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.media.StillImageLoader;
	import com.notsosimpleton.SectionsManager;
	import com.notsosimpleton.data.ProjectData;
	import com.notsosimpleton.events.NavigationEvent;
	import com.notsosimpleton.modules.ClientNavMod;
	import com.notsosimpleton.sections.AbstractSection;
	import com.notsosimpleton.sections.SectionReference;
	import com.notsosimpleton.sections.overlay.FullContentViewer;
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.type.MenuButton;
	import com.notsosimpleton.visualButton.type.SimpletonEventButton;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;

	public class Showcase extends AbstractSection
	{	
		private var _close:AbstractButton;
		private var _previousClient:AbstractButton;
		private var _nextClient:AbstractButton;
		private var _previousProject:AbstractButton;
		private var _nextProject:AbstractButton;
		private var _nextPhoto:AbstractButton;
		private var _previousPhoto:AbstractButton;
		
		private var _clientLogoNavHolder:MovieClip;
		private var _projectNavBtns:Array;
		private var _projectNavAssets:Array;
		private var _mainContentHolder:MovieClip;
		
		private var _subTitle:TextField;
		private var _details:TextField;
		private var _summary:TextField;
		private var _photoPosition:TextField;
		
		private var _projectLinkText:TextField;
		private var _externalLink:Sprite;
		
		private var _clientHeaders:MovieClip;
		private var _isInExistance:Boolean = false;
		
		private var _clientBuildOutFrameDest:Number;
		
		private var _clientNavModule:ClientNavMod;
		private var _stillImageLoader:StillImageLoader = new StillImageLoader(378/2);
		
		private var _xmlRef:Number;
		private var _photoTracker:int = 0;
		
		private var _fullContentViewer:FullContentViewer = new FullContentViewer();
		
		private var _navigationURL:String;
		
		public function Showcase(asset:Class)
		{
			super(asset);
			_externalLink = _asset.externalLink;
			_projectLinkText = _asset.externalLink.textLabel;
			_externalLink.addChild(_projectLinkText);
			_externalLink.mouseChildren = false;
			_externalLink.mouseEnabled = true;
			_externalLink.buttonMode = true;
			_externalLink.addEventListener(MouseEvent.CLICK, onExternalLinkClick);
		}
		
		private function onExternalLinkClick(e:MouseEvent):void
		{
			navigateToURL(new URLRequest("http://"+_navigationURL));	
		}
		
		override public function init():void
		{
			if(!_isInExistance)
			{
				initializeAssets();
			}
			
			addChild(_close);
			_close.x = 5;
			_close.y = 5;
			addChild(_previousClient);
			addChild(_nextClient);
			addChild(_previousProject);
			addChild(_nextProject);
			_close.buildAssetIn();
			
			//addChild(_externalLink);
			addChild(_subTitle);
			addChild(_details);
			addChild(_summary);
			addChild(_clientHeaders);
			addChild(_mainContentHolder);
			_mainContentHolder.addChild(_stillImageLoader);
			
			_stillImageLoader.addEventListener(Event.OPEN, imageLoadInProgress);
			_stillImageLoader.addEventListener(Event.COMPLETE, imageLoadInFinished);
			
			addChild(_clientLogoNavHolder);
			_clientLogoNavHolder.addChild(_clientNavModule);
			_clientNavModule.buildInClientLogo(ProjectData.currentID);
			
			_mainContentHolder.buttonMode = true;
			_mainContentHolder.addEventListener(MouseEvent.CLICK, onMainContentClick);
			stage.addChild(_fullContentViewer);
			
			setContent(0);
			
			addChild(_nextPhoto);
			addChild(_previousPhoto);
			addChild(_externalLink);
		}
		
		private function imageLoadInProgress(e:Event):void
		{
			SectionsManager.isInTransition = true;	
		}
		
		private function imageLoadInFinished(e:Event):void
		{
			SectionsManager.isInTransition = false;	
		}
		
		override public function buildOut():void
		{
			super.buildOut();
			revealAssets(0);
			_close.buildAssetOut();
			Tweener.addTween(_clientHeaders, {_frame:_clientBuildOutFrameDest, time:0.5});
			Tweener.addTween(_subTitle, {_text:"", time:0.5, transition:"linear"});
			Tweener.addTween(_details, {_text:"", time:0.5, transition:"linear"});
			Tweener.addTween(_summary, {_text:"", time:0.5, transition:"linear"});
			Tweener.addTween(_stillImageLoader, {alpha:0, time:0.5, onComplete:clearImageLoader});
			Tweener.addTween(_photoPosition, {_text:"", time:0.2, transition:"linear"});
			_clientNavModule.buildOutClientLogo(false);
		}
		
		private function clearImageLoader():void
		{
			_stillImageLoader.clearImg();
			
			//Make sure the image loader is visible for the next time the
			//showcase is called to view
			_stillImageLoader.alpha = 1;
		}
		
		private function showcaseNavigation(e:NavigationEvent):void
		{
			switch(e.type)
			{
				case NavigationEvent.NEXT_CLIENT		: 	_clientNavModule.navigateClientLogo(true);		break;
				case NavigationEvent.PREVIOUS_CLIENT	: 	_clientNavModule.navigateClientLogo(false); 	break;
				case NavigationEvent.NEXT_PROJECT		: 	alterXMLRef(true);								break;
				case NavigationEvent.PREVIOUS_PROJECT	: 	alterXMLRef(false);    						 	break;
				case NavigationEvent.NEXT_PHOTO			: 	setContent(_xmlRef, false, false, true);		break;
				case NavigationEvent.PREVIOUS_PHOTO		: 	setContent(_xmlRef, false, false, false);		break;
				case NavigationEvent.ONE				: 	setContent(0, false);							break;
				case NavigationEvent.TWO				: 	setContent(1, false);							break;
				case NavigationEvent.THREE				: 	setContent(2, false);							break;
				case NavigationEvent.FOUR				: 	setContent(3, false);							break;
				case NavigationEvent.FIVE				: 	setContent(4, false);							break;
				case NavigationEvent.SIX				: 	setContent(5, false);							break;
			}
		}
		
		private function initializeAssets():void
		{
			_close = new MenuButton(Back, SectionReference.SPLASH_SCREEN);
			//Crazy event architecture where the event is passed in as a string, and then retrieved again 
			//after being dispatched from the base type and travelling up the heirarchy.
			_previousClient = new SimpletonEventButton(PreviousClient, NavigationEvent.PREVIOUS_CLIENT);
			_nextClient = new SimpletonEventButton(NextClient, NavigationEvent.NEXT_CLIENT);
			_nextPhoto = new SimpletonEventButton(NextClient, NavigationEvent.NEXT_PHOTO);
			_previousPhoto = new SimpletonEventButton(PreviousClient, NavigationEvent.PREVIOUS_PHOTO);
			
			_previousProject = new SimpletonEventButton(PreviousProject, NavigationEvent.PREVIOUS_PROJECT);
			_nextProject = new SimpletonEventButton(NextProject, NavigationEvent.NEXT_PROJECT);
			
			var projBtn1:AbstractButton = new SimpletonEventButton(Page1, NavigationEvent.ONE);
			var projBtn2:AbstractButton = new SimpletonEventButton(Page2, NavigationEvent.TWO);
			var projBtn3:AbstractButton = new SimpletonEventButton(Page3, NavigationEvent.THREE);
			var projBtn4:AbstractButton = new SimpletonEventButton(Page4, NavigationEvent.FOUR);
			var projBtn5:AbstractButton = new SimpletonEventButton(Page5, NavigationEvent.FIVE);
			var projBtn6:AbstractButton = new SimpletonEventButton(Page6, NavigationEvent.SIX);
			
			_projectNavBtns = [projBtn1, projBtn2, projBtn3, projBtn4, projBtn5, projBtn6];
			//_externalLink = new URLButton(_asset.externalLink, "");
			
			positionAssetsHardCoded();
			
			projBtn1.addEventListener(NavigationEvent.ONE, showcaseNavigation);
			projBtn2.addEventListener(NavigationEvent.TWO, showcaseNavigation);
			projBtn3.addEventListener(NavigationEvent.THREE, showcaseNavigation);
			projBtn4.addEventListener(NavigationEvent.FOUR, showcaseNavigation);
			projBtn5.addEventListener(NavigationEvent.FIVE, showcaseNavigation);
			projBtn6.addEventListener(NavigationEvent.SIX, showcaseNavigation);
			
			_previousClient.addEventListener(NavigationEvent.PREVIOUS_CLIENT, showcaseNavigation);
			_nextClient.addEventListener(NavigationEvent.NEXT_CLIENT, showcaseNavigation);
			_previousProject.addEventListener(NavigationEvent.PREVIOUS_PROJECT, showcaseNavigation);
			_nextProject.addEventListener(NavigationEvent.NEXT_PROJECT, showcaseNavigation);
			_clientNavModule = new ClientNavMod(ProjectData.currentID);
			_clientNavModule.addEventListener(NavigationEvent.MODULE_CLICKED, setContent)
			_previousPhoto.addEventListener(NavigationEvent.PREVIOUS_PHOTO, showcaseNavigation);
			_nextPhoto.addEventListener(NavigationEvent.NEXT_PHOTO, showcaseNavigation);
			
			_subTitle = _asset.subTitle;
			_details = _asset.details;
			_summary = _asset.summary;
			_clientHeaders = _asset.clientHeaders;
			_mainContentHolder = _asset.mainContentHolder;
			_clientLogoNavHolder = _asset.clientLogoNavHolder;
			_photoPosition = _asset.photoPosition;
			
			_isInExistance = true;
		}
		
		private function alterXMLRef(increase:Boolean):void
		{
			var projectNumber:Number = ProjectData.getCurrentClient().project.length() - 1;
			if(increase && _xmlRef < projectNumber)
			{
				_xmlRef++;	
			}
			else if(increase && _xmlRef == projectNumber)
			{
				_xmlRef = 0;
			}
			else if(!increase && _xmlRef > 0)
			{
				_xmlRef--;
			}
			else if(!increase && _xmlRef == 0)
			{
				_xmlRef = projectNumber;
			}
			setContent(_xmlRef, false);
		}
		
		private function setContent(xmlRef:Number, isNewClient:Boolean = true, resetTracker:Boolean = true, increment:Boolean = false):void
		{	
			revealAssets();
			if(!xmlRef){ xmlRef = 0;}
			_xmlRef = xmlRef;
			var projectData:XML = ProjectData.getCurrentClient(); 
			var projectNumber:Number = projectData.project.length();
			var id:String = projectData.@id; 
			var subTitleTxt:String = projectData.project[xmlRef].@title;
			var summaryTxt:String = projectData.project[xmlRef].text;
			var detailsTxt:String = projectData.project[xmlRef].details.role.@value + "\n" + projectData.project[xmlRef].details.desc.@value + "\n" + projectData.project[xmlRef].details.date.@value;
			var assets:Array = new Array();
			var url:String = projectData.project[xmlRef].link.@url;
			_navigationURL = url;
			
			url == "" ? _externalLink.mouseEnabled = false : _externalLink.mouseEnabled = true;
			
			//Long conditional used for navigating the photos for a particular project
			if(resetTracker)
			{
				_photoTracker = 0;
			}
			else 
			{
				if(increment)
				{
					 if(_photoTracker == projectData.project[xmlRef].asset.length() -1)
					 {
						_photoTracker = 0;	 	
					 }
					 else
					 {
					 	_photoTracker++;
					 }
				}
				else
				{
					 if(_photoTracker == 0)
					 {
						_photoTracker = projectData.project[xmlRef].asset.length() -1;	 	
					 }
					 else
					 {
					 	_photoTracker--;
					 }
				}
				
			}	
			
			//The following assignments use comparison operators to determin the boolean value of visible
			//Not sure why, but the visibility of assets in the movie clip called "Content" also need to be set 
			
			_nextPhoto.visible = projectData.project[xmlRef].asset.length() > 1;
			_previousPhoto.visible = projectData.project[xmlRef].asset.length() > 1;
			_nextProject.visible = projectNumber > 1;
			_previousProject.visible = projectNumber > 1;
			
			//collect all of the assets
			for(var i:Number = 0; i < projectData.project[xmlRef].asset.length(); i++)
			{
				assets.push(projectData.project[xmlRef].asset[i].@thumb);
			}
			
			//add the correct number of project menu buttons
			for(var j:Number = 0; j < projectNumber; j++)
			{
				addChild(_projectNavBtns[j]);
			}
			
			//make the cooresponding assets visible
			for(var k:Number = 0; k < 6; k++)
			{
				//If there is only one project, there is no need to present a projNav button
				//	"projectNumber > 1" evaluates to true or false 
				_projectNavBtns[k].visible = k < projectNumber ? projectNumber > 1 : false;
				_projectNavBtns[k].alpha = k < projectNumber ? 1 : 0;
				
				//Scales each nav asset according to active
				_projectNavBtns[k].scaleX = k == xmlRef ? 1.5 : 1;
				_projectNavBtns[k].scaleY = k == xmlRef ? 1.5 : 1; 
				_projectNavBtns[k].enabled = k == xmlRef ? false : true;
			}
			
			_stillImageLoader.loadNewImg(assets[_photoTracker], 302/2);
			
			//Set photo tracker string
			var photoPosText:String =  " " + (_photoTracker + 1).toString() + " of " + (projectData.project[xmlRef].asset.length()).toString();
			
			
			if(isNewClient)
			{
				_clientHeaders.gotoAndStop(id);
				_clientBuildOutFrameDest = _clientHeaders.currentFrame;
				_clientHeaders.play();
			}
			Tweener.addTween(_subTitle, {_text:subTitleTxt, time:1, transition:"linear"});
			Tweener.addTween(_details, {_text:detailsTxt, time:1, transition:"linear"});
			Tweener.addTween(_summary, {_text:summaryTxt, time:1, transition:"linear"});
			Tweener.addTween(_photoPosition, {_text:photoPosText, time:0.4, transition:"linear"});
			Tweener.addTween(_projectLinkText, {_text:url, time:0.4, transition:"linear"});
		}
		
		private function revealAssets(a:Number = 1):void
		{
			for(var i:Number = 0; i < 6; i++)
			{
				_projectNavBtns[i].alpha = 0;
			}
			_nextProject.alpha = a;
			_previousProject.alpha = a;
			_nextPhoto.alpha = a;
			_previousPhoto.alpha = a;
			_nextClient.alpha = a;
			_previousClient.alpha = a;
		}
		
		private function positionAssetsHardCoded():void
		{
			//++++++++++++++++++++++++ OOOOOOHHHHHH THE HORROR!!!!!!!!!!!!!!!++++++++++++++++++++//
			//++++++++++++++++++++++++ OOOOOOHHHHHH THE HORROR!!!!!!!!!!!!!!!++++++++++++++++++++//
			//++++++++++++++++++++++++ OOOOOOHHHHHH THE HORROR!!!!!!!!!!!!!!!++++++++++++++++++++//
			
			for(var i:int = 0; i<_projectNavBtns.length; i++)
			{
				_projectNavBtns[i].y = 420;
				_projectNavBtns[i].x = 388 + (12*i);
			}
			
			_previousProject.x = 222;
			_nextProject.x = 486;
			_previousProject.y = _nextProject.y = 415;
			
			_nextClient.x = 177;
			_nextClient.scaleX = _nextClient.scaleY = _previousClient.scaleX = _previousClient.scaleY = _nextPhoto.scaleX =  _nextPhoto.scaleY = _previousPhoto.scaleX = _previousPhoto.scaleY = 1.5;
			_previousClient.x = 12;
			_nextClient.y = _previousClient.y = 125;
			
			_nextPhoto.x = 470;
			_previousPhoto.x = 370;
			_nextPhoto.y = _previousPhoto.y = 440;
			
			_nextPhoto.scaleX =_nextPhoto.scaleY = _previousPhoto.scaleX = _previousPhoto.scaleY = 1.2;
			
			_stillImageLoader.x += 6;
			_stillImageLoader.y += 1;
			//++++++++++++++++++++++++ OOOOOOHHHHHH THE HORROR!!!!!!!!!!!!!!!++++++++++++++++++++//
			//++++++++++++++++++++++++ OOOOOOHHHHHH THE HORROR!!!!!!!!!!!!!!!++++++++++++++++++++//
			//++++++++++++++++++++++++ OOOOOOHHHHHH THE HORROR!!!!!!!!!!!!!!!++++++++++++++++++++//
		}
		
		private function onMainContentClick(e:MouseEvent):void
		{
			_fullContentViewer.buildIn(_xmlRef, _photoTracker);
		}
		
		override public function position():void
		{
			super.position();
			//useing a accessor in the FullContentViewer class to ensure the _fullContentViewer has access 
			//to the stage before calling the positionAssets function which uses a stage instance in its algorythm
			if(_fullContentViewer.addedTostage) 
			{
				_fullContentViewer.positionAssets();
			}
		}
		
	}
}