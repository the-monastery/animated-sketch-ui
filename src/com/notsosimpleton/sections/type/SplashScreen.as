package com.notsosimpleton.sections.type
{
	import com.notsosimpleton.data.ProjectData;
	import com.notsosimpleton.events.NavigationEvent;
	import com.notsosimpleton.sections.AbstractSection;
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.type.ClientButton;
	
	import flash.display.MovieClip;
	
	public class SplashScreen extends AbstractSection
	{
		
		private var _buttonArray:Array = new Array();
		private var _clipTracker:Number = 0;
		
		public function SplashScreen(asset:Class)
		{
			super(asset);
			var _clientArray:XMLList = ProjectData.getXML().clients.client.@id;
			for(var i:int = 0; i < _clientArray.length(); i++)
			{
				//current refers to the name of all the movieclips in _asset
				//The movieclips should all be referenced in the config xml file
				_clipTracker++;
				var current:MovieClip = _asset.getChildByName(_clientArray[i]) as MovieClip;
				var client:AbstractButton = new ClientButton(current, i);
				_buttonArray.push(client);
			}
			
		}
			
		override public function buildOut():void
		{
			for(var i:int = 0; i < _buttonArray.length; i++)
			{
				MovieClip(_buttonArray[i]).enabled = false;
				AbstractButton(_buttonArray[i]).buildAssetOut();
				AbstractButton(_buttonArray[i]).addEventListener(NavigationEvent.BUILD_OUT_COMPLETE, removeClip);
			}
		}
		
		private function removeClip(e:NavigationEvent):void
		{
			removeChild(ClientButton(e.target));
			_clipTracker--;
			_clipTracker == 0 ? dispatchEvent(new NavigationEvent(NavigationEvent.BUILD_OUT_COMPLETE)) : "";
		}
		
		override public function buildIn():void
		{
			_asset.visible = true;
			for(var i:int = 0; i < _buttonArray.length; i++)
			{
				addChild(_buttonArray[i]);
				AbstractButton(_buttonArray[i]).buildAssetIn();
			}
			
			_clipTracker = _buttonArray.length;
			position();
			dispatchEvent(new NavigationEvent(NavigationEvent.BUILD_IN_COMPLETE));
		}
		
		override public function init():void
		{
			
		}
		
	}
}