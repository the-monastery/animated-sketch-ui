package com.notsosimpleton.sections.type
{
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.type.URLButton;
	import com.notsosimpleton.visualButton.type.URLLoopButton;
	
	public class Collaborators extends CollaboratorsHolder
	{
		private var _trent:AbstractButton;
		private var _geoff:AbstractButton;
		private var _dan:AbstractButton;
		private var _fatoa:AbstractButton;
		private var _michael:AbstractButton;
		private var _dave:AbstractButton;
		private var _nicholas:AbstractButton;
		private var _luke:AbstractButton;
		private var _pelletron:AbstractButton;
		private var _anita:AbstractButton;
		private var _sandra:AbstractButton;
		private var _zoe:AbstractButton;
		private var _erik:AbstractButton;
		private var _james:AbstractButton;
		
		public function Collaborators()
		{
			_trent = new URLLoopButton(trent,"http://www.meatlegs.com");
			_geoff = new URLLoopButton(geoff,"http://www.champagnevalentine.com");
			_dan = new URLLoopButton(dan, "http://www.everyonediesfilms.com");
			_fatoa = new URLLoopButton(fatoa, "http://mamafatou.com/");
			_michael = new URLLoopButton(michael, "http://www.writingmjb.com");
			_dave = new URLLoopButton(dave, "http://www.dronebrain.com");
			_nicholas = new URLLoopButton(nicholas, "http://selectedWork.ghostmonk.com");
			_luke = new URLLoopButton(luke, "http://www.collapsicon.net");
			_pelletron = new URLLoopButton(pelletron, "http://www.pelletron.org");
			_anita = new URLLoopButton(anita, "http://www.champagnevalentine.com");
			_sandra = new URLLoopButton(sandra, "http://www.girlatwork.com");
			_zoe = new URLLoopButton(zoe, "http://www.linkedin.com/in/zoecurnoe");
			_erik = new URLLoopButton(erik, "http://www.flickr.com/photos/notsosimpleton/48137958/sizes/o/");
			_james = new URLLoopButton(james, "http://www.thebathwater.com/");
			
			addChild(_trent);
			addChild(_geoff);
			addChild(_dan);
			addChild(_fatoa);
			addChild(_michael);
			addChild(_dave);
			addChild(_nicholas);
			addChild(_luke);
			addChild(_pelletron);
			addChild(_anita);
			addChild(_sandra);
			addChild(_zoe);
			addChild(_erik);
			addChild(_james);
		}
		
		public function buildOut():void
		{
			_trent.buildAssetOut();
			_geoff.buildAssetOut();
			_dan.buildAssetOut();
			_fatoa.buildAssetOut();
			_michael.buildAssetOut()
			_dave.buildAssetOut();
			_nicholas.buildAssetOut();
			_luke.buildAssetOut();
			_pelletron.buildAssetOut();
			_anita.buildAssetOut();
			_sandra.buildAssetOut();
			_zoe.buildAssetOut();
			_erik.buildAssetOut();
			_james.buildAssetOut();
		}
		
		public function buildIn():void
		{
			_trent.buildAssetIn();
			_geoff.buildAssetIn();
			_dan.buildAssetIn();
			_fatoa.buildAssetIn();
			_michael.buildAssetIn()
			_dave.buildAssetIn();
			_nicholas.buildAssetIn();
			_luke.buildAssetIn();
			_pelletron.buildAssetIn();
			_anita.buildAssetIn();
			_sandra.buildAssetIn();
			_zoe.buildAssetIn();
			_erik.buildAssetIn();
			_james.buildAssetIn();
		}
	}
}