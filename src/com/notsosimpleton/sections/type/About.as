package com.notsosimpleton.sections.type
{
	import caurina.transitions.Tweener;
	
	import com.notsosimpleton.data.ProjectData;
	import com.notsosimpleton.sections.AbstractSection;
	import com.notsosimpleton.sections.SectionReference;
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.type.MenuButton;
	import com.notsosimpleton.visualButton.type.URLButton;
	
	import fl.controls.UIScrollBar;
	
	import flash.text.TextField;

	public class About extends AbstractSection
	{
		
		private var _close:AbstractButton;
		private var _bioLink:AbstractButton;
		private var _portfolioLink:AbstractButton;
		private var _cvLink:AbstractButton;
		
		private var _summary:String = "";
		private var _collaborators:String = "";
		private var _info:String = "";
		
		private var _summaryField:TextField;
		private var _collaboratorsHolder:Collaborators;
		private var _infoField:TextField;
		
		private var _scrollBar:UIScrollBar;
		
		public function About(asset:Class)
		{
			super(asset);
			_close = new MenuButton(Back, SectionReference.SPLASH_SCREEN);
			_bioLink = new URLButton(BioLink, "http://www.notsosimpleton.com/assets/bio.pdf");
			_cvLink = new URLButton(ResumeLink, "http://www.notsosimpleton.com/assets/resume.pdf");
			_portfolioLink = new URLButton(PortfolioLink, "http://www.notsosimpleton.com");
			
			_summary = ProjectData.getXML().about.summary;
			_collaborators = ProjectData.getXML().about.collaborators;
			_info = ProjectData.getXML().about.info;
			
			_summaryField = _asset.summary;
			_collaboratorsHolder = new Collaborators();
			_infoField = _asset.info;
			
			_bioLink.x = 430;
			_bioLink.y = 325;
			_cvLink.x = 305;
			_cvLink.y = _bioLink.y;
			_portfolioLink.x = 276;
			_portfolioLink.y = 440;
			
			_portfolioLink.scaleX = _portfolioLink.scaleY = 0.8;
			
			_scrollBar = new UIScrollBar();
			_scrollBar.x = 491;
			_scrollBar.y = 36;
			/*_cvLink.scaleX = _cvLink.scaleY = 0.95;
			_bioLink.scaleX = _bioLink.scaleY = 0.95;*/
		}
		
		override public function init():void
		{
			addChild(_close);
			_close.x = 10;
			_close.y = 10;
			
			addChild(_cvLink);
			addChild(_bioLink);
			addChild(_portfolioLink);
			
			addChild(_summaryField);
			addChild(_collaboratorsHolder);
			addChild(_infoField);			
			addChild(_scrollBar);
			
			_collaboratorsHolder.x = 98;
			_collaboratorsHolder.y = 233;
			
			_close.buildAssetIn();
			_cvLink.buildAssetIn();
			_bioLink.buildAssetIn();
			_portfolioLink.buildAssetIn();
			_collaboratorsHolder.buildIn();
			
			_infoField.htmlText = _info;
			var infoTweenText:String = _infoField.text;
			_infoField.htmlText = "";
			
			Tweener.addTween(_summaryField, {_text:_summary, time:0.5, transition:"linear"});
			Tweener.addTween(_infoField, {_text:infoTweenText, time:0.5, transition:"linear", onComplete:setInfoText});
			
			_scrollBar.alpha = 0;
		}
		
		private function setInfoText():void
		{
			_infoField.htmlText = _info;
			initScroller();
		}
		
		override public function buildOut():void
		{
			super.buildOut();
			_close.buildAssetOut();
			_cvLink.buildAssetOut();
			_bioLink.buildAssetOut();
			_portfolioLink.buildAssetOut();
			_collaboratorsHolder.buildOut();
			
			Tweener.addTween(_summaryField, {_text:"", time:0.5, transition:"linear"});
			Tweener.addTween(_infoField, {_text:"", time:0.5, transition:"linear"});
			Tweener.addTween(_scrollBar,{alpha:0, time:0.5})
		}
		
		private function initScroller():void
		{
			_summaryField.maxScrollV != 1 ? Tweener.addTween(_scrollBar,{alpha:1, time:0.5}):_scrollBar.alpha = 0;
			_scrollBar.setStyle("scrollBarWidth",6);
			_scrollBar.scaleY = 1.4;
			_scrollBar.scrollTarget = _summaryField;
			_scrollBar.scrollPosition = 0;
		}
		
	}
}