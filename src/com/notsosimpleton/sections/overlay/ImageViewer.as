package com.notsosimpleton.sections.overlay
{
	import com.ghostmonk.media.StillImageLoader;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class ImageViewer extends Sprite
	{
		private var _loader:StillImageLoader = new StillImageLoader();
		private var _width:Number;
		private var _height:Number;
		
		public function get thisWidth():Number
		{
			return _width;
		}
		
		public function get thisHeight():Number
		{
			return _height;
		}
		
		public function ImageViewer()
		{
			addChild(_loader);
			_loader.addEventListener(Event.INIT, onImageInit);	
			_loader.addEventListener(Event.COMPLETE, onImageComplete);
		}
		
		public function loadImage(img:String, yPosition:Number):void
		{
			_loader.loadNewImg(img, yPosition, true);
			_loader.bufferX = stage.stageWidth/2;
		}
		
		public function clear():void
		{
			_loader.clearImg();
			
		}
		
		private function onImageInit(e:Event):void
		{
			///thisHeight and thisWidth passed along a chain... StillImageLoader -> ImageViewer -> FullContentViewer
			_width = _loader.thisWidth;
			_height = _loader.thisHeight;
			dispatchEvent(e);
		}
		
		private function onImageComplete(e:Event):void
		{
			dispatchEvent(e);
		}
		
	}
}