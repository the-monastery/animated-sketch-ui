package com.notsosimpleton.sections.overlay
{
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.events.VideoPlayerEvent;
	import com.notsosimpleton.events.NavigationEvent;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	[Event(name="fullScreen", type="com.notsosimpleton.events.NavigationEvent")]
	[Event(name="play", type="com.ghostmonk.events.VideoPlayerEvent")]
	[Event(name="update", type="com.ghostmonk.events.VideoPlayerEvent")]
	
	public class VideoController extends ControlPanel
	{
		private var _playToggle:MovieClip;
		private var _fullScreen:MovieClip;
		private var _scrubber:MovieClip;
		private var _isPlaying:Boolean = false;
		private var _progressBar:MovieClip;
		private var _scrubBarPercent:Number;
		
		public static const FULL_SCREEN:String = "fullScreen";
		
		public function getScrubPercent():Number{return _scrubBarPercent};
		
		public function VideoController()
		{
			_progressBar = this.progressBar;
			_playToggle = this.playToggle;
			_fullScreen = this.fullScreen;
			_scrubber = this.scrubHead;
			
			_playToggle.stop();
			_fullScreen.stop();
			
			_playToggle.buttonMode = true;
			_fullScreen.buttonMode = true;
			_scrubber.buttonMode = true;
			
			addChild(_progressBar);
			addChild(_playToggle);
			addChild(_fullScreen);
			addChild(_scrubber);
			
			_playToggle.addEventListener(MouseEvent.MOUSE_OVER, onPlayMouseOver);
			_playToggle.addEventListener(MouseEvent.CLICK, onPlayClick);
			
			_fullScreen.addEventListener(MouseEvent.MOUSE_OVER, onFullScreenMouseOver);
			_fullScreen.addEventListener(MouseEvent.CLICK, onFullScreenClick);
			
			_scrubber.addEventListener(MouseEvent.MOUSE_DOWN, onScrubMouseDown);
		}
		
		public function updateVideoProgress(playPercent:Number, downloadPercent:Number):void
		{
			_progressBar.scaleX = downloadPercent;
			 _scrubber.x = playPercent != 0 ? 511 * playPercent + progressBar.x : _progressBar.x;
		}
		
		public function reset():void
		{
			_isPlaying = false;
			_playToggle.gotoAndStop("play");
			_scrubber.x = _progressBar.x;
		}
		
		private function onScrubMouseDown(e:MouseEvent):void
		{
			_scrubber.removeEventListener(MouseEvent.MOUSE_DOWN, onScrubMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, onScrubMouseUp);
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onScrubMouseMove);
		}
		
		private function onScrubMouseUp(e:MouseEvent):void
		{
			_scrubber.addEventListener(MouseEvent.MOUSE_DOWN, onScrubMouseDown);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onScrubMouseUp);
			
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, onScrubMouseMove);
		}
		
		private function onScrubMouseMove(e:MouseEvent):void
		{
			_scrubber.x = Math.max(_progressBar.x, Math.min(this.mouseX, _progressBar.x + _progressBar.width));
			_scrubBarPercent = (_scrubber.x - _progressBar.x)/511;
			dispatchEvent(new VideoPlayerEvent(VideoPlayerEvent.UPDATE));
		}
		
		private function onPlayClick(e:MouseEvent):void
		{
			_isPlaying = !_isPlaying;
			dispatchEvent(new VideoPlayerEvent(VideoPlayerEvent.PLAY));
			Tweener.removeTweens(_playToggle);
			_isPlaying ? _playToggle.gotoAndStop("pause") : _playToggle.gotoAndStop("play");
		}
		
		private function onPlayMouseOver(e:MouseEvent):void
		{
			_playToggle.removeEventListener(MouseEvent.MOUSE_OVER, onPlayMouseOver);
			_playToggle.addEventListener(MouseEvent.MOUSE_OUT, onPlayMouseOut);
			Tweener.removeTweens(_playToggle);
			_playToggle.play();
		}
		
		private function onPlayMouseOut(e:MouseEvent):void
		{
			_playToggle.addEventListener(MouseEvent.MOUSE_OVER, onPlayMouseOver);
			_playToggle.removeEventListener(MouseEvent.MOUSE_OUT, onPlayMouseOut);
			_isPlaying ? Tweener.addTween(_playToggle, {_frame:11, time:0.5, transition:"linear"}) : Tweener.addTween(_playToggle, {_frame:0, time:0.5, transition:"linear"});
		}
		
		private function onFullScreenClick(e:MouseEvent):void
		{
			Tweener.removeTweens(_fullScreen);
			dispatchEvent(new NavigationEvent(NavigationEvent.FULL_SCREEN));	
		}
		
		private function onFullScreenMouseOver(e:MouseEvent):void
		{
			_fullScreen.removeEventListener(MouseEvent.MOUSE_OVER, onFullScreenMouseOver);
			_fullScreen.addEventListener(MouseEvent.MOUSE_OUT, onFullScreenMouseOut);
			Tweener.removeTweens(_fullScreen);
			_fullScreen.play();
		}
		
		private function onFullScreenMouseOut(e:MouseEvent):void
		{
			_fullScreen.addEventListener(MouseEvent.MOUSE_OVER, onFullScreenMouseOver);
			_fullScreen.removeEventListener(MouseEvent.MOUSE_OUT, onFullScreenMouseOut);
			Tweener.addTween(_fullScreen, {_frame:0, time:0.5, transition:"linear"});
		}
		
	}
}