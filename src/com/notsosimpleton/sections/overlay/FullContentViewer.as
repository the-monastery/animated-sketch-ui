package com.notsosimpleton.sections.overlay
{
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.events.VideoPlayerEvent;
	import com.notsosimpleton.data.ProjectData;
	import com.notsosimpleton.events.NavigationEvent;
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.type.SimpletonEventButton;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class FullContentViewer extends Sprite
	{
		
		private var _videoContainer:VideoContainer = new VideoContainer();
		private var _imageViewer:ImageViewer = new ImageViewer();
		private var _backGround:Sprite = new Sprite();
		private var _addedToStage:Boolean = false;
		private var _isActive:Boolean = false;
		private var _projectRef:int;
		private var _assetRef:int;
		
		private var _navBtnNext:AbstractButton = new SimpletonEventButton(NextClient, NavigationEvent.NEXT_PHOTO);
		private var _navBtnPrevious:AbstractButton = new SimpletonEventButton(PreviousClient, NavigationEvent.PREVIOUS_PHOTO);
		private var _navBtnClose:AbstractButton = new SimpletonEventButton(CloseBtn, NavigationEvent.CLOSE);
		
		private var _tweenSprite:Sprite;
	
		public function FullContentViewer()
		{
			_backGround.graphics.beginFill(0xffffff,0.9);
			_backGround.graphics.drawRect(0,0,1,1);
			_backGround.graphics.endFill();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			//_backGround.addEventListener(MouseEvent.CLICK, closeShowcase);
			_navBtnNext.addEventListener(NavigationEvent.NEXT_PHOTO, navigatePhoto);
			_navBtnPrevious.addEventListener(NavigationEvent.PREVIOUS_PHOTO, navigatePhoto);
			_navBtnClose.addEventListener(NavigationEvent.CLOSE, closeShowcase);
			
			_navBtnNext.scaleX = _navBtnNext.scaleY = _navBtnPrevious.scaleX = _navBtnPrevious.scaleY = 1.5;
		}
		
		private function navigatePhoto(e:NavigationEvent):void
		{
			e.type == NavigationEvent.NEXT_PHOTO ? loadImage(true) : loadImage(false) ;
		}
		
		public function buildIn(projectRef:int, assetRef:int):void
		{
			_projectRef = projectRef;
			_assetRef = assetRef;
			Tweener.addTween(this, {y:0, time:0.5, transition:"linear", onComplete:buildInContent});
			
			_navBtnClose.alpha = _navBtnNext.alpha = _navBtnPrevious.alpha = 0;
			_isActive = true;
		}
		
		public function positionAssets():void
		{
			_backGround.width = stage.stageWidth;
			_backGround.height = stage.stageHeight;
			
			_imageViewer.x = (stage.stageWidth - _imageViewer.thisWidth)/2;
			_videoContainer.x = (stage.stageWidth - _videoContainer.width)/2;
			
			if(!_isActive)
			{
				this.y = -stage.stageHeight;
				_videoContainer.y = -_backGround.height;
				_imageViewer.y = -_backGround.height;
			}
			else
			{
				_tweenSprite.y = (stage.stageHeight-_tweenSprite.height)/2;
			}
		}
		
		public function get addedTostage():Boolean
		{
			return _addedToStage;
		}
		
		private function onAddedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addChild(_backGround);
			addChild(_videoContainer);
			addChild(_imageViewer);
			_imageViewer.addChild(_navBtnNext);
			_imageViewer.addChild(_navBtnPrevious);
			
			positionAssets();
			y = -stage.stageHeight;
			_addedToStage = true;
		}
		
		private function buildInContent():void
		{
			var project:XML = ProjectData.getCurrentClient().project[_projectRef];
			var assetType:String = project.asset[_assetRef].@type;
			
			if(assetType == "video")
			{
				_tweenSprite = _videoContainer;
				_videoContainer.addChild(_navBtnClose);
				_videoContainer.loadVideo(project.asset[_assetRef].@url);
				_videoContainer.addEventListener(VideoPlayerEvent.READY, onVideoPlayerReady);
			}
			else
			{
				_tweenSprite = _imageViewer;
				_imageViewer.addChild(_navBtnClose);
				_imageViewer.loadImage(project.asset[_assetRef].@url, stage.stageHeight/2);
				_imageViewer.addEventListener(Event.INIT, onImageInit);
				Tweener.addTween(_tweenSprite, {y:(stage.stageHeight - _tweenSprite.height)/2, time:0.5})
			}  
			
			
		}
		
		private function onVideoPlayerReady(e:VideoPlayerEvent):void
		{
			_videoContainer.x = (stage.stageWidth - _videoContainer.width)/2;
			Tweener.addTween(_tweenSprite, {y:(stage.stageHeight - _tweenSprite.height)/2, time:0.5});
			_navBtnClose.x = -_navBtnClose.width - 5;
			_navBtnClose.y = 10;
			_navBtnClose.alpha = 1;
		}
		
		private function loadImage(next:Boolean):void
		{
			var project:XML = ProjectData.getCurrentClient().project[_projectRef];
			_tweenSprite = _imageViewer;
			
			if(next && _assetRef == project.asset.length() - 1)
			{
				_assetRef = 0;
			}
			else if(next && _assetRef < project.asset.length() - 1)
			{
				_assetRef++;
			}
			else if(!next && _assetRef == 0)
			{
				_assetRef = project.asset.length() - 1;
			}
			else
			{
				_assetRef--;
			}
			_imageViewer.loadImage(project.asset[_assetRef].@url, stage.stageHeight/2);
			_imageViewer.addEventListener(Event.INIT, onImageInit);
		}
		
		private function onImageInit(e:Event):void
		{
			//thisHeight and thisWidth passed along a chain... StillImageLoader -> ImageViewer -> FullContentViewer
			_imageViewer.x = (stage.stageWidth - _imageViewer.thisWidth)/2;
			_imageViewer.y = (stage.stageHeight - _imageViewer.thisHeight)/2;
			if( ProjectData.getCurrentClient().project[_projectRef].asset.length() > 1)
			{
				_navBtnClose.alpha = _navBtnNext.alpha = _navBtnPrevious.alpha = 1;
				_navBtnNext.enableBtn(true);
				_navBtnPrevious.enableBtn(true);
			}
			else
			{
				_navBtnClose.alpha = 1;
				_navBtnNext.enableBtn(false);
				_navBtnPrevious.enableBtn(false);
			}
			_navBtnClose.x = -_navBtnClose.width - 5;
			_navBtnClose.y = 10;
			_navBtnNext.x = _imageViewer.thisWidth + 15; 
			_navBtnPrevious.x =_navBtnClose.x;
			_navBtnPrevious.y = _imageViewer.thisHeight/2;
			_navBtnNext.y = _navBtnPrevious.y;
		}
		
		private function closeShowcase(e:Event):void
		{
			Tweener.addTween(_tweenSprite, {y:-_backGround.height, time:0.2, transition:"linear", onComplete:clearAssets});
			Tweener.addTween(this, {y:-stage.stageHeight, time:0.2, delay:0.2, transition:"linear"});
			_isActive = false;
		}
		
		private function clearAssets():void
		{
			_imageViewer.clear();
			_videoContainer.clearVideo();
		}
	}
}