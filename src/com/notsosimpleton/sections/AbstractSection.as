package com.notsosimpleton.sections
{
	import caurina.transitions.Tweener;
	
	import com.notsosimpleton.events.NavigationEvent;
	
	import flash.display.MovieClip;
	
	[Event(name="buildOutComplete", type="com.notsosimpleton.events.NavigationEvent")]

	
	public class AbstractSection extends MovieClip
	{
		protected var _asset:MovieClip;
		
		public function AbstractSection(asset:Class)
		{
			_asset = new asset;
			addChild(_asset);
			_asset.stop();
			_asset.visible = false;
		}
		
		public function buildOut():void
		{
			Tweener.addTween(_asset, {_frame:0, time:1, onComplete:dispatchComplete, transition:"linear"});
		}
		
		private function dispatchComplete():void
		{
			dispatchEvent(new NavigationEvent(NavigationEvent.BUILD_OUT_COMPLETE));
		}
		
		public function buildIn():void
		{
			_asset.visible = true;
			position();
			Tweener.addTween(_asset, {_frame:_asset.totalFrames, time:1, onComplete:loadContent, transition:"linear"});
		}
		
		private function loadContent():void
		{
			init();
			dispatchEvent(new NavigationEvent(NavigationEvent.BUILD_IN_COMPLETE));
		}
		
		public function init():void
		{
			throw new Error("You must override the init() function when inheriting AbstractSection");
		}
		
		public function position():void
		{
			x = (stage.stageWidth - this.width)/2;
			y = (stage.stageHeight - this.height)/2 + 20;
		}

	}
}