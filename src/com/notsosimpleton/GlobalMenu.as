package com.notsosimpleton
{
	import com.notsosimpleton.sections.SectionReference;
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.type.MenuButton;
	import com.notsosimpleton.visualButton.type.URLButton;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class GlobalMenu extends GlobalNavigation
	{		
		private var _aboutBtn:AbstractButton;
		private var _newsBtn:AbstractButton;
		private var _emailBtn:AbstractButton;
		private var _productionsBtn:AbstractButton;
		
		public function GlobalMenu()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void
		{
			_aboutBtn = new MenuButton(about, SectionReference.ABOUT);
			//_newsBtn = new MenuButton(news, SectionReference.CURRENT_AFFAIRS);
			_emailBtn = new URLButton(email, "mailto:myron@notsosimpleton.com");
			_productionsBtn = new MenuButton(productions, SectionReference.SPLASH_SCREEN);
			
			addChild(_aboutBtn);
			//addChild(_newsBtn);
			addChild(_productionsBtn);
			addChild(_emailBtn);
		}
		
	}
}