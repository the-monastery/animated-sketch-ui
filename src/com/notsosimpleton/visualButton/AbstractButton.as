package com.notsosimpleton.visualButton
{
	
	import caurina.transitions.Tweener;
	
	import com.notsosimpleton.events.NavigationEvent;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	[Event(name="buildOutComplete", type="com.notsosimpleton.events.NavigationEvent")]
	
	public class AbstractButton extends MovieClip
	{
		protected var _clickBehaviour:IButtonClick;
		private var _asset:MovieClip;
		private var _restFrame:Number;
		private var _isButtonActive:Boolean;
		
		private var _loopFrame:Number;
		
		public function AbstractButton(asset:*)
		{
			_asset = asset is Class ? new asset : asset; 
			addChild(_asset);
			_asset.buttonMode = true;
			_asset.addEventListener(MouseEvent.CLICK, whenClicked);
			_asset.addEventListener(MouseEvent.MOUSE_OVER, onRollOver);
			_asset.addEventListener(MouseEvent.MOUSE_OUT, onRollOut);
			
			calculateRestFrame();
			buildAssetIn();
		}
		
		private function calculateRestFrame():void
		{
			//A bit of a  hack to get the final location of the rollout frame
			_asset.gotoAndStop("over");
			_restFrame = _asset.currentFrame - 1;
		}
		
		public function calculateLoopFrame():void
		{
			_asset.gotoAndStop("loop");
			_loopFrame = _asset.currentFrame;
		}
		
		private function whenClicked(e:MouseEvent):void
		{
			_clickBehaviour.onClick(e);
		}
		
		private function onRollOver(e:MouseEvent):void
		{
			_isButtonActive = true;
			//This variable ensures the time for the frame animation 
			//is always relative to the number of frames
			var _animTime:Number = (_asset.totalFrames - _asset.currentFrame)*0.03;
			
			Tweener.addTween(_asset, {_frame:_asset.totalFrames, time:_animTime, transition:"linear", onComplete:comeToRest});
		}
		
		private function onRollOut(e:MouseEvent):void
		{
			_isButtonActive = false;
		}
		
		public function comeToRest():void
		{
			if(_isButtonActive)
			{
				_loopFrame ? _asset.gotoAndStop(_loopFrame) : _asset.gotoAndStop(_restFrame);
				var _animTime:Number = (_asset.totalFrames - _asset.currentFrame)*0.03;
				Tweener.addTween(_asset, {_frame:_asset.totalFrames, time:_animTime, transition:"linear", onComplete:comeToRest});
			}
			else
			{
				_asset.gotoAndStop(_restFrame);
			}
			
		}
		
		public function buildAssetOut():void
		{
			//This variable ensures the time for the frame animation 
			//is always relative to the number of frames
			var _animTime:Number = _asset.currentFrame*0.03;
			Tweener.addTween(_asset, {_frame:0, time:_animTime, transition:"linear", onComplete:broadcastEvent});
		}
		
		public function buildAssetIn():void
		{
			_asset.gotoAndStop(0);
			//This variable ensures the time for the frame animation 
			//is always relative to the number of frames
			var _animTime:Number = _restFrame*0.03;
			Tweener.addTween(_asset, {_frame:_restFrame, time:_animTime, transition:"linear"});
		}
		
		private function broadcastEvent():void
		{
			dispatchEvent(new NavigationEvent(NavigationEvent.BUILD_OUT_COMPLETE));
		}
		
		public function enableBtn(enable:Boolean):void
		{
			if(enable)
			{
				_asset.addEventListener(MouseEvent.CLICK, whenClicked);
				_asset.buttonMode = true;
			}
			else
			{
				_asset.removeEventListener(MouseEvent.CLICK, whenClicked);
				_asset.buttonMode = false;
			}
			
		}

	}
}