package com.notsosimpleton.visualButton.behaviour
{
	import com.notsosimpleton.visualButton.IButtonClick;
	
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;

	public class ToURL implements IButtonClick
	{
		private var _url:String;
		private var _window:String;
		
		public function set url(location:String):void
		{
			_url = location;
		}
		
		public function ToURL(url:String, window:String)
		{
			_url = url;
			_window = window;
		}

		public function onClick(e:MouseEvent):void
		{
			navigateToURL(new URLRequest(_url), _window);
		}
		
	}
}