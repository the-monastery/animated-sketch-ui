package com.notsosimpleton.visualButton.behaviour
{
	import com.notsosimpleton.SectionsManager;
	import com.notsosimpleton.data.ProjectData;
	import com.notsosimpleton.events.NavigationEvent;
	import com.notsosimpleton.sections.SectionReference;
	import com.notsosimpleton.visualButton.IButtonClick;
	
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;

	[Event (name="navigateToSection", type="com.notsosimpleton.events.NavigationEvent")]

	public class InjectXML extends EventDispatcher implements IButtonClick
	{
		
		private var _section:Number;
		private var _modButton:Boolean;
		
		public function InjectXML(section:Number, mod:Boolean)
		{
			_modButton = mod;
			_section = section;
		}

		public function onClick(e:MouseEvent):void
		{
			if(!_modButton){
				SectionsManager.getInstance().buildOutSection(SectionReference.SHOWCASE);
			}
			ProjectData.setCurrentClient(_section);
			//trace(ProjectData.getXML().clients.client[_section]);
		}
		
	}
}