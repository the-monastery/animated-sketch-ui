package com.notsosimpleton.visualButton.behaviour
{
	import com.notsosimpleton.events.NavigationEvent;
	import com.notsosimpleton.visualButton.IButtonClick;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	[Event (name="previousClient", type="com.notsosimpleton.events.NavigationEvent")]
	[Event (name="nextClient", type="com.notsosimpleton.events.NavigationEvent")]
	[Event (name="previousProject", type="com.notsosimpleton.events.NavigationEvent")]
	[Event (name="nextProject", type="com.notsosimpleton.events.NavigationEvent")]
	
	public class DispatchSimpletonEvent extends MovieClip implements IButtonClick
	{
		private var _event:String;
		
		public function DispatchSimpletonEvent(event:String)
		{
			_event = event;
		}
		
		public function onClick(e:MouseEvent):void
		{
			super.dispatchEvent(new NavigationEvent(_event));
		}

	}
}