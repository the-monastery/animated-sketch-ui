package com.notsosimpleton.visualButton.behaviour
{
	import com.notsosimpleton.SectionsManager;
	import com.notsosimpleton.events.NavigationEvent;
	import com.notsosimpleton.visualButton.IButtonClick;
	
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;

	public class ToSiteLocation implements IButtonClick
	{
		private var _destination:String;
		
		public function ToSiteLocation(destination:String)
		{
			_destination = destination;
		}

		public function onClick(e:MouseEvent):void
		{
			SectionsManager.getInstance().buildOutSection(_destination);
			
		}
		
	}
}