package com.notsosimpleton.visualButton.type
{
	import com.notsosimpleton.events.NavigationEvent;
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.behaviour.DispatchSimpletonEvent;

	public class SimpletonEventButton extends AbstractButton
	{
		public function SimpletonEventButton(asset:*, event:String)
		{
			super(asset);
			
			_clickBehaviour = new DispatchSimpletonEvent(event);
			DispatchSimpletonEvent(_clickBehaviour).addEventListener(event, tracer);
		}
		
		private function tracer(e:NavigationEvent):void
		{
			dispatchEvent(new NavigationEvent(e.type));
		}
		
	}
}