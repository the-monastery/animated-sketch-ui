package com.notsosimpleton.visualButton.type
{
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.behaviour.ToURL;
	
	public class URLButton extends AbstractButton
	{
		
		public function URLButton(asset:*, url:String, window:String = "_blank")
		{
			super(asset);
			_clickBehaviour = new ToURL(url, window);
		}
	}
}