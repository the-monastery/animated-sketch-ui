package com.notsosimpleton.visualButton.type
{
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.behaviour.ToSiteLocation;
	
	public class MenuButton extends AbstractButton
	{
		public function MenuButton(asset:*, destination:String)
		{
			super(asset);
			_clickBehaviour = new ToSiteLocation(destination);
		}
		
	}
}