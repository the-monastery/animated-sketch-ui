package com.notsosimpleton.visualButton.type
{
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.behaviour.InjectXML;
	
	public class ClientButton extends AbstractButton
	{
		public function ClientButton(asset:*, section:Number, mod:Boolean = false)
		{
			super(asset);
			_clickBehaviour = new InjectXML(section, mod);
		}
		
	}
}