package com.notsosimpleton.visualButton.type
{
	import caurina.transitions.Tweener;
	
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.behaviour.ToURL;
	
	public class URLLoopButton extends AbstractButton
	{
		public function URLLoopButton(asset:*, url:String, window:String = "_blank")
		{
			
			super(asset);
			calculateLoopFrame();
			_clickBehaviour = new ToURL(url, window);
		}

	}
}