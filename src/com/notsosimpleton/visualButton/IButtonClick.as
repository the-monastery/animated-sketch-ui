package com.notsosimpleton.visualButton
{
	import flash.events.MouseEvent;
	
	public interface IButtonClick
	{
		function onClick(e:MouseEvent):void;
	}
}