package com.notsosimpleton.data
{
	public class ProjectData
	{
		private static var _data:XML;
		private static var _currentClient:XML;
		private static var _currentID:int;
		
		public static function get currentID():int
		{
			return _currentID;
		} 
		
		public static function getXML():XML
		{
			return _data;
		}
		
		public static function setXML(xml:XML):void
		{
			_data = xml;
		}
		
		public static function setCurrentClient(section:Number):void
		{
			_currentID = section;
			_currentClient = ProjectData.getXML().clients.client[section];
		}
		
		public static function getCurrentClient():XML
		{
			return _currentClient;
		}
		

	}
}