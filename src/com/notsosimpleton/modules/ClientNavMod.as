package com.notsosimpleton.modules
{
	import com.notsosimpleton.SectionsManager;
	import com.notsosimpleton.data.ProjectData;
	import com.notsosimpleton.events.NavigationEvent;
	import com.notsosimpleton.visualButton.AbstractButton;
	import com.notsosimpleton.visualButton.type.ClientButton;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class ClientNavMod extends MovieClip
	{
		private var _buttonArray:Array = new Array();
		private var _section:SplashPage = new SplashPage();
		private var _currentSecRef:Number;
		private var _lastChild:AbstractButton;
		
		private var _buildNew:Boolean;
		private var _isTransitioning:Boolean = false;
		
		[Event (name="moduleClicked", type="com.notsosimpleton.events.NavigationEvent")]
		
		public function ClientNavMod(secRef:Number)
		{
			_currentSecRef = secRef;
			
			var _clientArray:XMLList = ProjectData.getXML().clients.client.@id;
			for(var i:int = 0; i < _clientArray.length(); i++)
			{
				//current refers to the name of all the movieclips in _asset
				//The movieclips should all be referenced in the config xml file
				var current:MovieClip = _section.getChildByName(_clientArray[i]) as MovieClip;
				var client:AbstractButton = new ClientButton(current, i, true);
				_buttonArray.push(client);
				
				current.scaleX = 136/current.width;
				if(current.scaleX >= 1){current.scaleX = 1};
				current.scaleY = current.scaleX;
				
				current.x = (136 - current.width)/2;
				current.y = (92 - current.height)/2;
			}
			
			buildInClientLogo();
			
		}
		
		private function onClick(e:MouseEvent):void
		{
			dispatchEvent(new NavigationEvent(NavigationEvent.MODULE_CLICKED));
			//This comparison conditional is duplicated in "buildInClientLogo" because when I tried to 
			//create a seperate function, the functionality broke... not sure why, could be an order of operations
			//issue, I suspect functions are called in differing order.
			_currentSecRef == ProjectData.currentID ? removeEventListener(MouseEvent.CLICK, onClick) : addEventListener(MouseEvent.CLICK, onClick);
		}
		
		public function navigateClientLogo(increment:Boolean):void
		{
			buildOutClientLogo(true);
		
			//Logic used for looping through the button array
			if(_currentSecRef == 0 && !increment)
			{
				_currentSecRef = _buttonArray.length - 1;	
			}
			else if(_currentSecRef == _buttonArray.length -1 && increment)
			{
				_currentSecRef = 0;
			}
			else
			{
				increment ? _currentSecRef += 1 : _currentSecRef -= 1;		
			}
		}
		
		public function buildInClientLogo(secRef:Number = -1):void
		{
			SectionsManager.isInTransition = false;	
			if(secRef != -1){_currentSecRef = secRef;}
			addChild(_buttonArray[_currentSecRef]);
			AbstractButton(_buttonArray[_currentSecRef]).buildAssetIn();
			_currentSecRef == ProjectData.currentID ? removeEventListener(MouseEvent.CLICK, onClick) : addEventListener(MouseEvent.CLICK, onClick);
		}
		
		public function buildOutClientLogo(buildNew:Boolean):void
		{
			SectionsManager.isInTransition = true;
			_buildNew = buildNew;
			AbstractButton(_buttonArray[_currentSecRef]).buildAssetOut();
			_lastChild = _buttonArray[_currentSecRef];
			AbstractButton(_buttonArray[_currentSecRef]).addEventListener(NavigationEvent.BUILD_OUT_COMPLETE, removeLast);
		}
		
		private function removeLast(e:Event = null):void
		{
			if(_lastChild != null){ removeChild(_lastChild);}
			if(_buildNew){buildInClientLogo();}	
		}
		
	}
};