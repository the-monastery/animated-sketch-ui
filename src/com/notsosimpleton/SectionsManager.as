package com.notsosimpleton
{
	import com.notsosimpleton.events.NavigationEvent;
	import com.notsosimpleton.sections.AbstractSection;
	import com.notsosimpleton.sections.SectionReference;
	import com.notsosimpleton.sections.type.About;
	import com.notsosimpleton.sections.type.CurrentAffairs;
	import com.notsosimpleton.sections.type.Showcase;
	import com.notsosimpleton.sections.type.SplashScreen;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class SectionsManager extends Sprite
	{
		
		private static var _instance:SectionsManager;
		
		private var _showcase:AbstractSection = new Showcase(Content);
		//private var _newsSection:AbstractSection = new CurrentAffairs(News);
		private var _splashScreen:AbstractSection = new SplashScreen(SplashPage);
		private var _about:AbstractSection =  new About(InformationSection);
		
		private var _isAddedToStage:Boolean = false;
		
		private var _current:AbstractSection;
		private var _destination:String;
		
		//used in stageClicked() function
		private static var _isInTransition:Boolean = false;
		
		public static function set isInTransition(val:Boolean):void
		{
			_isInTransition = val;
		}
		
		public function SectionsManager()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_instance = this;
		}
		
		//Create a pseudo Singleton, onus on the developer to call getInstance
		public static function getInstance():SectionsManager
		{
			return _instance;
		}
		
		public function buildInSection():void
		{
			addChild(_current);
			_current.buildIn();
			_current.addEventListener(NavigationEvent.BUILD_IN_COMPLETE, onBuildInComplete);
		}
		
		private function onBuildInComplete(e:NavigationEvent):void
		{
			_isInTransition = false;
			_current.removeEventListener(NavigationEvent.BUILD_IN_COMPLETE, onBuildInComplete);
		}
		
		public function buildOutSection(dest:String):void
		{
			_isInTransition = true;
			_destination = dest;
			_current.buildOut();
			_current.addEventListener(NavigationEvent.BUILD_OUT_COMPLETE, sectionClear); 
		}
		
		public function reposition():void
		{
			_isAddedToStage ? _current.position() : "";
		}
		
		private function onAddedToStage(e:Event):void
		{
			_current = _splashScreen;
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_isAddedToStage = true;
			buildInSection();
			
			stage.addEventListener(MouseEvent.CLICK, absorbMouseEvents, true);
			stage.addEventListener(MouseEvent.MOUSE_OVER, absorbMouseEvents, true);
		}
		
		//The stage will absorb all mouseclicks while sections are in transition
		private function absorbMouseEvents(e:MouseEvent):void
		{
			_isInTransition ? e.stopPropagation() : "";
		}
		
		private function sectionClear(e:NavigationEvent):void
		{
			removeChild(_current);
			
			switch(_destination)
			{
				case SectionReference.ABOUT 			: 	_current 	= 	_about; 		break;
				//case SectionReference.CURRENT_AFFAIRS 	: 	_current 	= 	_newsSection; 	break;
				case SectionReference.SHOWCASE     		: 	_current 	= 	_showcase; 		break;
				case SectionReference.SPLASH_SCREEN 	: 	_current 	= 	_splashScreen; 	break;
			}
			
			buildInSection();
		}
		
	}
}