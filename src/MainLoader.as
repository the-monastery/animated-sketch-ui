package
{
	import caurina.transitions.*;
	import caurina.transitions.properties.DisplayShortcuts;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;

	[SWF (width=900, height=570, backgroundColor=0xffffff, frameRate=31)]
	public class MainLoader extends Sprite
	{
		
		private var _preloader:MovieClip = new Preloader();
		private var _mainLoader:Loader = new Loader();
		private var _percentDisplay:TextField;
		
		public function MainLoader()
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			DisplayShortcuts.init();
			_preloader.alpha = 0;

			stage.addEventListener(Event.RESIZE, onStageResize);
			_mainLoader.contentLoaderInfo.addEventListener(Event.OPEN, onOpen);
			_mainLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			_mainLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);			
			_mainLoader.load(new URLRequest("NotSoSimpleton.swf"));
		}
		
		private function onOpen(e:Event):void 
		{
		  	addChild(_preloader);
			onStageResize(e);
			Tweener.addTween(_preloader, {alpha:1, time:0.5});
		}
		
		private function onProgress(e:ProgressEvent):void {
			var percent:Number = e.bytesTotal != 0 ? e.bytesLoaded / e.bytesTotal : 0;
			_preloader.pbar.scaleX = percent;
		}
		
		private function onComplete(event:Event):void 
		{
			Tweener.addTween(_preloader.frame, {_frame:_preloader.frame.totalFrames, time:0.5, transition:"linear", onComplete:showContent});
		}
		
		private function onStageResize(e:Event):void
		{
			_preloader.x = stage.stageWidth/2;
			_preloader.y = stage.stageHeight/2;
		}
		
		private function showContent():void
		{
			removeChild(_preloader);
			addChild(_mainLoader);
		}
				
	}
}